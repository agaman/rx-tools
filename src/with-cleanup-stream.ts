import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const PipeWithCleanupStream = (cleanup$: Subject<void>) => <T>(stream: Observable<T>) => stream.pipe(
    takeUntil(cleanup$)
);
export type IPipeWithCleanupStream = ReturnType<typeof PipeWithCleanupStream>;

const TriggerCleanupStream = (cleanup$: Subject<void>) => (callback?: () => void) => () => {
    cleanup$.next();
    cleanup$.complete();

    if (callback) callback();
};
export type ITriggerCleanupStream = ReturnType<typeof TriggerCleanupStream>;

export const WithCleanupStream = () => {
    const cleanup$ = new Subject<void>();

    return {
        pipeWithCleanup: PipeWithCleanupStream(cleanup$),
        triggerCleanup: TriggerCleanupStream(cleanup$)
    };
};

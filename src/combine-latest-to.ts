import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

interface ICombineLatestToMap {
    [key: string]: any;
};

const MapArrayToKeys = <T>(arr: any[], keys: string[]) => keys.reduce((acc, key, idx) => {
    return {
        ...acc,
        [key]: arr[idx]
    };
}, {} as T);

export const CombineLatestTo = <T extends ICombineLatestToMap>(items: {[P in keyof T]: Observable<T[P]>}) => combineLatest(...Object.values(items)).pipe(
    map(values => MapArrayToKeys<T>(values, Object.keys(items)))
);
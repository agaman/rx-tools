import { timer } from 'rxjs';
import { map, take } from 'rxjs/operators';
import test from 'tape';

import {
    CombineLatestTo
} from '../src';

const RunTest = (name: string, runner: (t: test.Test, ...streams: any) => void) => test(name, _test => {
    const first$ = timer(0, 100).pipe(take(1), map(_ => 1));
    const second$ = timer(0, 150).pipe(take(1), map(_ => 2));

    runner(_test, first$, second$);
});

RunTest('CombineLatestTo', (t, first$, second$) => {
    const mapping = {
        first: first$,
        second: second$
    };

    CombineLatestTo<{
        first: number;
        second: number;
    }>(mapping).subscribe(value => {
        t.equals(value.first, 1, 'First stream value must be 1');
        t.equals(value.second, 2, 'Second stream value must be 2')
    });

    t.end();
});
